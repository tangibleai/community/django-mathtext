import re

from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from helpers.supabase_logging import prepare_message_data_for_logging
from helpers.intent_classification import create_intent_classification_model, retrieve_intent_classification_model, predict_message_intent
from mathtext.text2int import text2int


def build_nlu_response_object(type, data, confidence):
    """ Turns nlu results into an object to send back to Turn.io
    Inputs
    - type: str - the type of nlu run (integer or sentiment-analysis)
    - data: str/int - the student message
    - confidence: - the nlu confidence score (sentiment) or '' (integer)

    >>> build_nlu_response_object('integer', 8, 0)
    {'type': 'integer', 'data': 8, 'confidence': 0}

    >>> build_nlu_response_object('sentiment', 'POSITIVE', 0.99)
    {'type': 'sentiment', 'data': 'POSITIVE', 'confidence': 0.99}
    """
    return {'type': type, 'data': data, 'confidence': confidence}


def check_for_keywords(message_text):
    """ Process a student's message using basic fuzzy text comparison

    >>> check_for_keywords("exit")
    {'type': 'intent', 'data': 'exit', 'confidence': 1.0}
    >>> check_for_keywords("exi")  # doctest: +ELLIPSIS   
    {'type': 'intent', 'data': 'exit', 'confidence': 0...}
    >>> check_for_keywords("eas")  # doctest: +ELLIPSIS
    {'type': 'intent', 'data': 'easy', 'confidence': 0...}
    >>> check_for_keywords("hard")
    {'type': 'intent', 'data': '', 'confidence': 0}
    >>> check_for_keywords("hardier")  # doctest: +ELLIPSIS
    {'type': 'intent', 'data': 'harder', 'confidence': 0...}
    >>> check_for_keywords("I'm tired")  # doctest: +ELLIPSIS
    {'type': 'intent', 'data': 'tired', 'confidence': 1.0}
    """
    label = ''
    ratio = 0
    nlu_response = {'type': 'intent', 'data': label, 'confidence': ratio}
    keywords = [
        'easier',
        'exit',
        'harder',
        'hint',
        'next',
        'stop',
        'tired',
        'tomorrow',
        'finished',
        'help',
        'easier',
        'easy',
        'support',
        'skip',
        'menu'
    ]

    try:
        tokens = re.findall(r"[-a-zA-Z'_]+", message_text.lower())
    except AttributeError:
        tokens = ''

    for keyword in keywords:
        try:
            tok, score = process.extractOne(keyword, tokens, scorer=fuzz.ratio)
        except:
            score = 0

        if score > 80:
            nlu_response['data'] = keyword
            nlu_response['confidence'] = score / 100

    return nlu_response


def evaluate_message_with_nlu(message_data):
    """ Process a student's message using NLU functions and send the result
    
    >>> evaluate_message_with_nlu({"author_id": "57787919091", "author_type": "OWNER", "contact_uuid": "df78gsdf78df", "message_body": "8", "message_direction": "inbound", "message_id": "dfgha789789ag9ga", "message_inserted_at": "2023-01-10T02:37:28.487319Z", "message_updated_at": "2023-01-10T02:37:28.487319Z"})
    {'answer': {'data': 8, 'confidence': 0}, 'intent': {'data': '', 'confidence': ''}}

    >>> evaluate_message_with_nlu({"author_id": "57787919091", "author_type": "OWNER", "contact_uuid": "df78gsdf78df", "message_body": "I am tired", "message_direction": "inbound", "message_id": "dfgha789789ag9ga", "message_inserted_at": "2023-01-10T02:37:28.487319Z", "message_updated_at": "2023-01-10T02:37:28.487319Z"})
    {'answer': {'data': '', 'confidence': ''}, 'intent': {'type': 'intent', 'data': 'tired', 'confidence': 1.0}}
    """
    # Keeps system working with two different inputs - full and filtered @event object
    try:
        message_text = message_data['message_body']
    except KeyError:
        message_data = {
            'author_id': message_data['message']['_vnd']['v1']['chat']['owner'],
            'author_type': message_data['message']['_vnd']['v1']['author']['type'],
            'contact_uuid': message_data['message']['_vnd']['v1']['chat']['contact_uuid'],
            'message_body': message_data['message']['text']['body'],
            'message_direction': message_data['message']['_vnd']['v1']['direction'],
            'message_id': message_data['message']['id'],
            'message_inserted_at': message_data['message']['_vnd']['v1']['chat']['inserted_at'],
            'message_updated_at': message_data['message']['_vnd']['v1']['chat']['updated_at'],
        }
        message_text = message_data['message_body']

    nlu_response = {
        'answer': {'data': '', 'confidence': ''},
        'intent': {'data': '', 'confidence': ''}
    }

    intent_api_response = check_for_keywords(message_text)
    if intent_api_response['data']:
        nlu_response['intent'] = intent_api_response
        prepare_message_data_for_logging(message_data, nlu_response)
        return nlu_response

    number_api_resp = text2int(message_text.lower())
    nlu_response['answer'] = {'data': number_api_resp, 'confidence': 0}
    if number_api_resp == 32202:
        # Run intent classification with logistic regression model
        nlu_response['intent'] = predict_message_intent(message_text)

    prepare_message_data_for_logging(message_data, nlu_response)
    return nlu_response
