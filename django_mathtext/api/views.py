"""FastAPI endpoint
To run locally use 'uvicorn app:app --host localhost --port 7860'
or
`python -m uvicorn app:app --reload --host localhost --port 7860`
"""

import json
import os
import requests
import sentry_sdk
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from json import JSONDecodeError
from logging import getLogger
from mathactive_django.views import start_quiz
from mathtext.text2int import text2int
from helpers.nlu import evaluate_message_with_nlu as v1_evaluate_message_with_nlu
from helpers.v2_nlu.v2_nlu import evaluate_message_with_nlu as v2_evaluate_message_with_nlu
from helpers.v2_conversation_manager.v2_conversation_manager import manage_conversation_response as v2_manage_conversation_response
from helpers.v3_conversation_manager.v3_conversation_manager import manage_conversation_response as v3_manage_conversation_response
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.utils import BadDsn

log = getLogger(__name__)

try:
    sentry_sdk.init(
        dsn="https://examplePublicKey@o0.ingest.sentry.io/0",
        # ...
        integrations=[
            DjangoIntegration(
                transaction_style='url',
                middleware_spans=True,
                signals_spans=False,
            ),
        ],
    )
except BadDsn:
    pass


@csrf_exempt
@require_http_methods(["GET"])
def home(request):
    return render(request, "home.html")


@csrf_exempt
@require_http_methods(["POST"])
def text2int_ep(request):
    payload = json.loads(request.body)
    ml_response = text2int(payload["content"])
    content = {"message": ml_response}
    return JsonResponse(content)


@csrf_exempt
@require_http_methods(["POST"])
def v2_programmatic_message_manager(request):
    """
    Calls conversation management function to determine the next state

    Input
    request.body: dict - message data for the most recent user response
    {
        "author_id": "+47897891",
        "contact_uuid": "j43hk26-2hjl-43jk-hnk2-k4ljl46j0ds09",
        "author_type": "OWNER",
        "message_body": "a test message",
        "message_direction": "inbound",
        "message_id": "ABJAK64jlk3-agjkl2QHFAFH",
        "message_inserted_at": "2022-07-05T04:00:34.03352Z",
        "message_updated_at": "2023-02-14T03:54:19.342950Z",
    }

    Output
    context: dict - the information for the current state
    {
        "user": "47897891",
        "state": "welcome-message-state",
        "bot_message": "Welcome to Rori!",
        "user_message": "",
        "type": "ask"
    }
    """
    data_dict = json.loads(request.body)
    nlu_response = v2_manage_conversation_response(data_dict)
    return JsonResponse(nlu_response)


@csrf_exempt
@require_http_methods(["POST"])
def programmatic_message_manager(request):
    """
    Calls conversation management function to determine the next state

    Input
    request.body: dict - message data for the most recent user response
    {
        "author_id": "+47897891",
        "contact_uuid": "j43hk26-2hjl-43jk-hnk2-k4ljl46j0ds09",
        "author_type": "OWNER",
        "message_body": "a test message",
        "message_direction": "inbound",
        "message_id": "ABJAK64jlk3-agjkl2QHFAFH",
        "message_inserted_at": "2022-07-05T04:00:34.03352Z",
        "message_updated_at": "2023-02-14T03:54:19.342950Z",
    }

    Output
    context: dict - the information for the current state
    {
        "user": "47897891",
        "state": "welcome-message-state",
        "bot_message": "Welcome to Rori!",
        "user_message": "",
        "type": "ask"
    }
    """
    data_dict = json.loads(request.body)
    context = v3_manage_conversation_response(data_dict)
    return JsonResponse(context)


@csrf_exempt
@require_http_methods(["POST"])
def extend_conversation_claim(request):
    """ Receives a POST request from Turn.io and 1) extends the lease by 5 minutes and 2) sends a message

    X-Turn-Claim-Extend in headers extends lease.
    """
    data_bytes = request.body
    data_decoded = data_bytes.decode()
    data_json = json.loads(data_decoded)

    headers = {
        'Authorization': f"Bearer {os.environ.get('TURN_AUTHENTICATION_TOKEN')}",
        'X-Turn-Claim-Extend': request.headers["X-Turn-Claim"],
        'Content-Type': 'application/json'
    }

    data = {
        "preview_url": False,
        "to": data_json['contacts'][0]['wa_id'],
        "type": "text",
        "text": {
            "body": "The claim has been extended."
        }
    }

    r = requests.post('https://whatsapp.turn.io/v1/messages', data=json.dumps(data), headers=headers)

    return JsonResponse(data)


@csrf_exempt
@require_http_methods(["POST"])
def receive_response_ep(request):
    data_dict = json.loads(request.body)

    try: 
        message_data = {
            "author_id": data_dict['messages'][0]["_vnd"]["v1"]["chat"]["owner"],
            "contact_uuid": data_dict['messages'][0]["_vnd"]["v1"]["chat"]["contact_uuid"],
            "author_type": "OWNER",
            "message_body": data_dict['messages'][0]["interactive"]["button_reply"]["title"],
            "message_direction": "inbound",
            "message_id": data_dict['messages'][0]["id"],
            "message_inserted_at": data_dict['messages'][0]["_vnd"]["v1"]["chat"]["inserted_at"],
            "message_updated_at": data_dict['messages'][0]["_vnd"]["v1"]["chat"]["updated_at"],
        }

        context_data = {
            "contact_id": data_dict['messages'][0]["_vnd"]["v1"]["chat"]["owner"],
            "contact_uuid": data_dict['messages'][0]["_vnd"]["v1"]["chat"]["contact_uuid"],
            "author_type": "OWNER",
            "current_intent": '',
            "curriculum_state": '',
            "microlesson_state": '',
            "bot_message": '',
            "user_message": data_dict['messages'][0]["interactive"]["button_reply"]["title"],
            "type": 'ask'
        }

        data_json = {
            'message_data': message_data,
            'context_data': context_data
        }

        response = v3_manage_conversation_response(data_json)
        print("RESPONSE")
        print(response)
    except:
        response = {}

    return JsonResponse(response)


@csrf_exempt
@require_http_methods(["POST"])
def evaluate_user_message_with_nlu_api(request):
    """ Calls nlu evaluation and returns the nlu_response

    Input
    - request.body: json - message data for the most recent user response

    Output
    - int_data_dict or sent_data_dict: dict - the type of NLU run and result
      {'type':'integer', 'data': '8', 'confidence': 0}
    """
    log.info(f'Received request: {request}')
    log.info(f'Request header: {request.headers}')
    payload = json.loads(request.body)
    # request_body = await request.body()
    log.info(f'Request body: {payload}')
    # request_body_str = payload.decode()
    # log.info(f'Request_body_str: {request_body_str}')

    try:
        # data_dict = await request.json()
        data_dict = json.loads(request.body)
    except JSONDecodeError:
        log.error(f'Request.json failed: {dir(request)}')
        data_dict = {}
    message_data = data_dict.get('message_data')

    if not message_data:
        log.error(f'Data_dict: {data_dict}')
        message_data = data_dict.get('message', {})

    data_dict = json.loads(request.body)
    message_data = data_dict.get('message_data', '')
    nlu_response = v1_evaluate_message_with_nlu(message_data)
    return JsonResponse(nlu_response)


@csrf_exempt
@require_http_methods(["POST"])
def v2_evaluate_user_message_with_nlu_api(request):
    """ Calls nlu evaluation and returns the nlu_response

    Input
    - request.body: json - message data for the most recent user response

    Output
    - int_data_dict or sent_data_dict: dict - the type of NLU run and result
      {'type':'integer', 'data': '8', 'confidence': 0}
    """
    log.info(f'Received request: {request}')
    log.info(f'Request header: {request.headers}')
    payload = json.loads(request.body)
    # request_body = await request.body()
    log.info(f'Request body: {payload}')
    # request_body_str = payload.decode()
    # log.info(f'Request_body_str: {request_body_str}')

    try:
        # data_dict = await request.json()
        data_dict = json.loads(request.body)
    except JSONDecodeError:
        log.error(f'Request.json failed: {dir(request)}')
        data_dict = {}
    message_data = data_dict.get('message_data')

    if not message_data:
        log.error(f'Data_dict: {data_dict}')
        message_data = data_dict.get('message', {})

    data_dict = json.loads(request.body)
    message_data = data_dict.get('message_data', '')
    nlu_response = v2_evaluate_message_with_nlu(message_data)
    return JsonResponse(nlu_response)


@csrf_exempt
@require_http_methods(["POST"])
def num_one(request):
    """
    Input:
    {
        "user_id": 1,
        "message_text": 5,
    }
    Output:
    {
        'messages':
            ["Let's", 'practice', 'counting', '', '', '46...', '47...', '48...', '49', '', '', 'After', '49,', 'what', 'is', 'the', 'next', 'number', 'you', 'will', 'count?\n46,', '47,', '48,', '49'],
        'input_prompt': '50',
        'state': 'question'
    }
    """
    res = start_quiz(request)
    return res
