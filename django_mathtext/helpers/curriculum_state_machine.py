"""module is never used"""

from transitions import Machine
from helpers.curriculum_mapper import build_curriculum_logic

all_states, all_transitions = build_curriculum_logic()


class CurriculumStateMachine(object):
    states = all_states

    transitions = all_transitions

    def __init__(
        self,
        initial_state='N1.1.1_G1',
    ):
        self.machine = Machine(
            model=self,
            states=CurriculumStateMachine.states,
            transitions=CurriculumStateMachine.transitions,
            initial=initial_state
        )


curriculum_state_machine = CurriculumStateMachine()
