import hashlib
import time

from django.contrib.auth.models import (
    UserManager,
    AbstractUser,
)
from django.contrib.sites.models import Site
from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.utils.translation import gettext as _     


class Contact(models.Model):
    id = models.BigAutoField(primary_key=True)
    project = models.IntegerField()
    original_contact_id = models.CharField(max_length=100, unique=True)
    urn = models.CharField(
        max_length=100, 
        black=True,
        null=True
    )
    language_code = models.CharField(max_length=100)
    contact_inserted_at = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    modified_at = models.DateTimeField(default=timezone.now)

    class Meta:
        managed = False
        db_table = 'contact'


class Message(models.Model):
    id = models.BigAutoField(primary_key=True)
    contact = models.IntegerField()
    original_message_id = models.CharField(max_length=100, unique=False)
    text = models.TextField()
    direction = models.CharField(max_length=100)
    sender_type = models.CharField(max_length=100)
    channel_type = models.CharField(max_length=100)
    message_inserted_at = models.DateTimeField(default=timezone.now)
    message_modified_at = models.DateTimeField(default=timezone.now)
    message_sent_at = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    modified_at = models.DateTimeField(default=timezone.now)
    nlu_response = models.JSONField(
        blank=True,
        null=True,
    )
    request_object = models.JSONField(
        blank=True,
        null=True,
    )

    class Meta:
        managed = False
        db_table = 'message'


class Project(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100, unique=False)
    created_at = models.DateTimeField(default=timezone.now)
    modified_at = models.DateTimeField(default=timezone.now)

    class Meta:
        managed = False
        db_table = 'project'


class StateMachines(models.Model):
    id = models.BigAutoField(primary_key=True)
    contact_uuid = models.CharField(
        max_length=100, 
        blank=True,
        null=True
    )
    addition3 =  models.JSONField(
        blank=True,
        null=True,
    )
    subtraction = models.JSONField(
        blank=True,
        null=True,
    )
    subtraction = models.JSONField(
        blank=True,
        null=True,
    )
    addition = models.JSONField(
        blank=True,
        null=True,
    )

    class Meta:
        managed = False
        db_table = 'state_machines'
