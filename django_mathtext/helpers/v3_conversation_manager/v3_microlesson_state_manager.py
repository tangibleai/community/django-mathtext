import base64
import os
import pickle
import json

from mathactive.generators import start_interactive_math
from mathactive.hints import generate_hint
from mathactive.microlessons import num_one
from helpers.math_quiz_fsm import MathQuizFSM
from helpers.math_subtraction_fsm import MathSubtractionFSM
from api.models import StateMachines
from django_mathtext.constants import SUPA
from logging import getLogger

log = getLogger(__name__)


def unpickle_and_unencode_state_machine(contact_state_machines):
    undump_encoded = base64.b64decode(
        contact_state_machines.data[0][type].encode('utf-8')
    )
    math_quiz_state_machine = pickle.loads(undump_encoded)
    return math_quiz_state_machine


def pickle_and_encode_state_machine(state_machine):
    dump = pickle.dumps(state_machine)
    dump_encoded = base64.b64encode(dump).decode('utf-8')
    return dump_encoded


def save_state_machine(
    state_machine,
    contact_uuid,
    existing_state_machine=False
):
    dump_encoded = json.dumps(state_machine)

    try:
        existing_state_machine = StateMachines.objects.filter(contact_uuid=contact_uuid).exists()

        if existing_state_machine:
            StateMachines.objects.filter(contact_uuid=contact_uuid).update(**{type: dump_encoded})
        else:
            state_machine_entry = StateMachines(contact_uuid=contact_uuid, **{type: dump_encoded})
            state_machine_entry.save()

        return "Success"

    except Exception as e:
        log.error(f'Failed to save state machine: {type} : {contact_uuid}')
        return "Error"

    # dump_encoded = pickle_and_encode_state_machine(state_machine)

    # if existing_state_machine:
    #     entry = SUPA.table('state_machines').update({
    #         f'{type}': dump_encoded
    #     }).eq(
    #         "contact_uuid", contact_uuid
    #     ).execute()
    #     return entry

    # entry = SUPA.table('state_machines').insert({
    #     'contact_uuid': contact_uuid,
    #     f'{type}': dump_encoded
    # }).execute()
    # return entry


microlesson_fsm_lookup_table = {
    'addition': MathQuizFSM(),
    'subtraction': MathSubtractionFSM()
}


def look_for_existing_quiz_state_machine(contact_uuid):
    try:
        contact_state_machines = StateMachines.objects.filter(contact_uuid=contact_uuid).values()
        return contact_state_machines

    except Exception as e:
        log.error(f'Failed to retrieve contact state machines: {contact_uuid}')
        return []



    # contact_state_machines = SUPA.table('state_machines').select("*").eq(
    #     "contact_uuid",
    #     contact_uuid
    # ).execute()
    # return contact_state_machines


def create_new_state_machine_instance(
    type,
    contact_uuid,
    any_state_machine_exists=False
):
    math_quiz_state_machine = microlesson_fsm_lookup_table[type]
    messages = [math_quiz_state_machine.response_text]
    save_state_machine = (
        math_quiz_state_machine,
        contact_uuid,
        any_state_machine_exists
    )
    return messages


def update_existing_state_machine_instance(
    type,
    contact_uuid,
    contact_state_machines,
    user_message
):
    math_quiz_state_machine = unpickle_and_unencode_state_machine(
        contact_state_machines
    )
    math_quiz_state_machine.student_answer = user_message
    math_quiz_state_machine.correct_answer = str(
        math_quiz_state_machine.correct_answer
    )
    save_state_machine = (
        math_quiz_state_machine,
        contact_uuid,
        True
    )
    messages = math_quiz_state_machine.validate_answer()
    return messages


def manage_math_quiz_fsm(user_message, contact_uuid, type):
    contact_state_machines = look_for_existing_quiz_state_machine(contact_uuid)
    existing_state_machine = True

    # No fsms exist - new user
    if contact_state_machines.data == []:
        messages = create_new_state_machine_instance(type, contact_uuid)

    # User has a record, but no state machine for this microlesson
    elif not contact_state_machines.data[0][type]:
        messages = create_new_state_machine_instance(type, contact_uuid, True)

    # User has a record and a state machine for this microlesson
    elif contact_state_machines.data[0][type]:
        messages = update_existing_state_machine_instance(
            type,
            contact_uuid,
            contact_state_machines,
            user_message
        )
    return messages


def create_message_package(messages, microlesson_state):
    input_prompt = messages.pop()
    message_package = {
        'messages': messages,
        'input_prompt': input_prompt,
        'microlesson_state': microlesson_state
    }
    return message_package


def retrieve_microlesson_content(
    context_data,
    user_message,
    microlesson_code,
    contact_uuid
):
    if context_data['microlesson_state'] == 'addition' or \
            microlesson_code == 'addition':
        messages = manage_math_quiz_fsm(
            user_message,
            contact_uuid,
            'addition'
        )

        if user_message == 'exit':
            state_label = 'exit'
        else:
            state_label = 'addition-question-sequence'

        message_package = create_message_package(
            messages,
            state_label
        )

    elif context_data['microlesson_state'] == 'addition2' or \
            microlesson_code == 'addition2':
        if user_message == 'harder' or user_message == 'easier':
            user_message = ''
        message_package = num_one.process_user_message(
            contact_uuid,
            user_message
        )
        message_package['microlesson_state'] = 'addition2'
        message_package['input_prompt'] = '?'

    elif context_data['microlesson_state'] == 'subtraction-question-sequence' or \
            user_message == 'subtract' or \
            microlesson_code == 'subtraction':
        messages = manage_math_quiz_fsm(
            user_message,
            contact_uuid,
            'subtraction'
        )

        if user_message == 'exit':
            state_label = 'exit'
        else:
            state_label = 'subtraction-question-sequence'

        message_package = create_message_package(
            messages,
            state_label
        )
    return message_package


curriculum_lookup_table = {
    'N1.1.1_G1': 'addition',
    'N1.1.1_G2': 'addition2',
    'N1.1.2_G1': 'subtraction'
}


def lookup_microlesson(next_state):
    microlesson = curriculum_lookup_table[next_state]
    return microlesson
