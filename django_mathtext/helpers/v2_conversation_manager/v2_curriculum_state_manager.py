import copy
import helpers.curriculum_state_machine as csm
from transitions import Machine


def determine_curriculum_state_position(current_curriculum_state, user_message):
    if not current_curriculum_state:
        current_curriculum_state = 'N1.1.1_G1'

    curriculum_machine_copy = copy.deepcopy(csm.curriculum_state_machine)
    curriculum_machine_copy.state = current_curriculum_state

    if user_message == 'easier':
        curriculum_machine_copy.left()
        next_state = curriculum_machine_copy.state
    elif user_message == 'harder':
        curriculum_machine_copy.right()
        next_state = curriculum_machine_copy.state
    else:
        next_state = current_curriculum_state
    
    return next_state