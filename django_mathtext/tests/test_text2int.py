import json
import pandas as pd
from pathlib import Path
from django.test import Client


# # The raw file URL has to be used for GitLab.
URL = "https://gitlab.com/tangibleai/community/mathtext/-/raw/main/mathtext/data/master_test_text2int.csv"

DATA_DIR = Path(__file__).parent.parent / "data"

DF = None
client = Client()


def setup_module(module):
    """setup any state specific to the execution of the given module."""
    global DF
    DF = pd.read_csv(URL)


def get_response_text2int(text):
    """Makes a post request to the endpoint"""
    r = None
    try:
        r = client.post("/text2int", data=json.dumps({"content": text}), content_type="application/json") \
            .json().get("message")
    except:
        pass
    return r


def test_endpoint_text2int():
    """Tests if endpoint is working"""
    response = client.post(
        "/text2int", data=json.dumps({"content": "fourteen"}), content_type="application/json"
    )
    assert response.status_code == 200


def test_acc_score_text2int():
    """Calculates accuracy score for endpoint"""

    DF["text2int"] = DF["input"].apply(func=get_response_text2int)
    DF["score"] = DF[["output", "text2int"]].apply(
        lambda row: row[0] == row[1],
        axis=1
    )
    print(DATA_DIR)
    DF.to_csv(f"{DATA_DIR}/text2int_results.csv", index=False)
    acc_score = DF["score"].mean().__round__(2)

    assert acc_score >= 0.5, f"Accuracy score: '{acc_score}'. Value is too low!"

