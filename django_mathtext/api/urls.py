from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("text2int", views.text2int_ep, name="text2int"),
    path("v2/manager", views.v2_programmatic_message_manager, name="v2_programmatic_message_manager"),
    path("v3/manager", views.programmatic_message_manager, name="programmatic_message_manager"),
    path("claim", views.extend_conversation_claim, name="extend_conversation_claim"),
    path("receive-response", views.receive_response_ep, name="receive_response_ep"),
    path("nlu", views.evaluate_user_message_with_nlu_api, name="nlu"),
    path("v2/nlu", views.v2_evaluate_user_message_with_nlu_api, name="v2_nlu"),
    path("num_one", views.num_one, name="num_one"),
]
