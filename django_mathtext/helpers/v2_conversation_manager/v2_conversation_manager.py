import json
import random
import requests

from django_mathtext.constants import TURN_AUTHENTICATION_TOKEN
from helpers.v2_conversation_manager.v2_curriculum_state_manager import determine_curriculum_state_position
from helpers.v2_conversation_manager.v2_microlesson_state_manager import lookup_microlesson, retrieve_microlesson_content
# from helpers.nlu import evaluate_message_with_nlu
from helpers.v2_nlu.v2_nlu import evaluate_message_with_nlu


def create_text_message(message_text, whatsapp_id):
    """ Fills a template with input values to send a text message to Whatsapp

    Inputs
    - message_text: str - the content that the message should display
    - whatsapp_id: str - the message recipient's phone number

    Outputs
    - message_data: dict - a preformatted template filled with inputs
    """
    message_data = {
        "preview_url": False,
        "recipient_type": "individual",
        "to": whatsapp_id,
        "type": "text",
        "text": {
            "body": message_text
        }
    }
    return message_data


def send_microlesson_content_messages(microlesson_content, whatsapp_id):
    """ Send each text message for the current state before a user input prompt

    Input:
    - microlesson_content: dict - the messages, input prompt, and state
    - whatsapp_id: str - the message recipient's phone number

    Output:
    - NA
    """
    headers = {
        'Authorization': f"Bearer {TURN_AUTHENTICATION_TOKEN}",
        'Content-Type': 'application/json'
    }
    for message in microlesson_content['messages']:
        data = create_text_message(message, whatsapp_id)
        r = requests.post(
            f'https://whatsapp.turn.io/v1/messages',
            data=json.dumps(data),
            headers=headers
        )


def manage_conversation_response(data_json):
    """ Calls functions necessary to determine message and context data """
    message_data = data_json['message_data']
    context_data = data_json['context_data']
    whatsapp_id = message_data['author_id']
    user_message = message_data['message_body']

    # Evaluate message with NLU
    # nlu_response = evaluate_message_with_nlu(message_data)

    next_state = determine_curriculum_state_position(
        context_data['curriculum_state'],
        user_message
    )

    # Manage microlesson state position
    microlesson_code = lookup_microlesson(next_state)
    microlesson_content = retrieve_microlesson_content(
        context_data,
        user_message,
        microlesson_code,
        context_data['contact_uuid']
    )

    # Message handling and response
    send_microlesson_content_messages(microlesson_content, whatsapp_id)

    updated_context = {
        "context": {
            "contact_id": whatsapp_id,
            "contact_uuid": context_data['contact_uuid'],
            "curriculum_state": next_state,
            "microlesson_state": microlesson_content['state'],
            "bot_message": microlesson_content['input_prompt'],
            "user_message": user_message,
            "type": 'ask'
        }
    }
    return updated_context
